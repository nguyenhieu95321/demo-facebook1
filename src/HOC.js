import React from 'react';

const Hoc = Component => ({...props}) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [isHover, setIsHover] = React.useState(false);
    const onMountEnter = () => {
        setIsHover(true);
    }
    const onMountLeaver = () => {
        setIsHover(false)
    }
    return (
        <div
            style={{
                opacity: isHover ? 0.5 : 1
            }}

            onMouseEnter={() => onMountEnter()}
            onMouseLeave={() => onMountLeaver()}
        >
            <Component {...props}/>
        </div>
    );
}

export default Hoc;