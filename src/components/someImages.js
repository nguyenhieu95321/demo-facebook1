import React, {Component} from 'react';

const SomeImages = (
    {src, width = 300, height = 200}
) => {
    return (
        <div
            className={"SomeImages"}
            style={{
                width: `${width}px`,
                height: `${height}px`,
                backgroundImage: `url(${src})`,
                backgroundSize: 'cover',
                backgroundPosition: "center",
                borderRadius: '8px'
            }}
        />
    );
}


export default SomeImages;