import React, {Component} from 'react';
import './style.css'
import {AppContext} from "../../Context/AppContext";

const ListDevice =  () =>  {
    const {theme} = React.useContext(AppContext)
    console.log(theme)
    return (
        <div className={theme}>
            <p>Device 1</p>
            <p>Device 1</p>
            <p>Device 1</p>
            <p>Device 1</p>
            <p>Device 1</p>
        </div>
    );
}

export default ListDevice;